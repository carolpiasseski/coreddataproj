//
//  ViewController.swift
//  CoreData
//
//  Created by Carolina Piasseski Marcelino Fernandes on 21/05/19.
//  Copyright © 2019 Carolina Piasseski Marcelino Fernandes. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var tvShows: [TVShow] = []
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        fetchTVShows()
    }

    @IBAction func didTapButton(_ sender: Any) {
        if let text = textField.text {
            saveNewTVShow(tvShowName: text)
            textField.text = nil
            
        }
    }
    func saveNewTVShow(tvShowName: String) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let context = appDelegate?.persistentContainer.viewContext {
            let tvShow = TVShow(context: context)
            tvShow.name = tvShowName
            tvShows.append(tvShow)
            tableView.reloadData()
            
            do {
                try context.save()
            }catch let error {
                print("Ocorreu um Erro \(error)")
            }
        }
    }
    
    func fetchTVShows() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let context = appDelegate?.persistentContainer.viewContext {
            let request:NSFetchRequest = TVShow.fetchRequest()
            
            do{
                tvShows = try context.fetch(request)
            }catch let error {
                print("Ocorreu um erro \(error)")
            }
        }
    }
}


extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tvShows.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TVShowCell", for: indexPath)
        cell.textLabel?.text = tvShows[indexPath.row].name
        
        return cell
    }
}

